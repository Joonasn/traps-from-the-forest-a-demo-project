﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    class _3D
    {

        GraphicsDeviceManager Graphics;
        GraphicsDevice device;
        SpriteBatch spriteBatch;
        SpriteFont font;
        Vector3 CamPosition, CamAtVector, CamUpVector, Camtarget;
        VertexPositionTexture[] floorParts, vertices, vertices2, vertices3, vertices4, vertices5;
        BasicEffect basicEffect;
        Effect effect;
        Texture2D floor, moon, tree;
        Texture2D[] Slav1 = new Texture2D[3];
        Texture2D[] Slav2 = new Texture2D[6];
        Color BGcolor = Color.DeepSkyBlue;

        int[] slavAnim = new int[] { 0, 0 };        // PASKAAAAAAA!!!!
        int[,] slavFrame = new int[2, 8] { { 0, 1, 2, 1, 0, 1, 2, 1 }, { 1, 2, 3, 2, 1, 4, 5, 4 } };

        float[] timer = new float[] { 0, 0, 0.8f, 0.5f };
        float[] timerMax = new float[] { 3, 2 };
        float AspectRatio, cameraFix = -2;

        float angle = 0, lerpingTime = 0.1f, dayTimer;
        int sunMoonLocation = 400;
        bool DayTime = true, attractMode = true;

        string ScrollingText = "Taa oli aika vaikee teha...... Ei mulla muuta. Eteenpain!";
        int ScrollTextLocation = 2200;

        Vector3 transformedReference;
        Matrix viewMatrix, WorldMatrix;
        Matrix projectionMatrix, projectionMatrix2;
        VertexBuffer vertexBuffer;

        public _3D(Texture2D MOON, Texture2D FLOOR, Texture2D TREE, SpriteFont FONT, Effect TEXT, Texture2D[] SLAAVI1, Texture2D[] SLAAVI2, GraphicsDeviceManager graphics, SpriteBatch SB)
        {
            Graphics = graphics;
            device = Graphics.GraphicsDevice;
            floor = FLOOR;
            moon = MOON;
            tree = TREE;
            font = FONT;
            effect = TEXT;

            spriteBatch = SB;

            for (int i = 0; i < 3; i++)
            {
                Slav1[i] = SLAAVI1[i];
            }

            for (int i = 0; i < 6; i++)
            {
                Slav2[i] = SLAAVI2[i];
            }

            floorParts = new VertexPositionTexture[6];
            floorParts[0].Position = new Vector3(-120, -120, -1f);
            floorParts[1].Position = new Vector3(-120, 120, -1f);
            floorParts[2].Position = new Vector3(120, -120, -1f);
            floorParts[3].Position = floorParts[1].Position;
            floorParts[4].Position = new Vector3(120, 120, -1f);
            floorParts[5].Position = floorParts[2].Position;

            int repetitions = 30;

            floorParts[0].TextureCoordinate = new Vector2(0, 0);
            floorParts[1].TextureCoordinate = new Vector2(0, repetitions);
            floorParts[2].TextureCoordinate = new Vector2(repetitions, 0);
            floorParts[3].TextureCoordinate = floorParts[1].TextureCoordinate;
            floorParts[4].TextureCoordinate = new Vector2(repetitions, repetitions);
            floorParts[5].TextureCoordinate = floorParts[2].TextureCoordinate;

            basicEffect = new BasicEffect(device);

            SetUpCamera();
            SetUpVertices();
        }

        public void SetUpCamera()
        {
            //CamPosition = new Vector3(0, -8, 0.5f);
            CamPosition = new Vector3(0, cameraFix, 0.5f);
            viewMatrix = Matrix.CreateLookAt(CamPosition, Camtarget, Vector3.Up);
            WorldMatrix = Matrix.CreateWorld(Camtarget, Vector3.Forward, Vector3.Up);
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, device.Viewport.AspectRatio, 1.0f, 200.0f);
        }

        public void UpdateCamera()
        {
            if (cameraFix > -8)
            {
                cameraFix -= 0.1f;
            }

            CamPosition = new Vector3(0, cameraFix, 0.5f);
        }

        public void SetUpVertices()
        {
            vertices = new VertexPositionTexture[6];

            vertices[0].Position = new Vector3(0f, 2f, 0f);
            vertices[1].Position = new Vector3(2f, -1f, 0f);
            vertices[2].Position = new Vector3(0f, -1f, 0f);
            vertices[3].Position = new Vector3(2f, -1f, 0f);
            vertices[4].Position = new Vector3(0f, 2f, 0f);
            vertices[5].Position = new Vector3(2f, 2f, 0f);

            vertices[0].TextureCoordinate = new Vector2(0, 0);
            vertices[1].TextureCoordinate = new Vector2(1, 1);
            vertices[2].TextureCoordinate = new Vector2(0, 1);
            vertices[3].TextureCoordinate = new Vector2(1, 1);
            vertices[4].TextureCoordinate = new Vector2(0, 0);
            vertices[5].TextureCoordinate = new Vector2(1, 0);

            vertices2 = new VertexPositionTexture[6];

            vertices2[0].Position = new Vector3(-2f, 1f, 0f);
            vertices2[1].Position = new Vector3(0f, -1f, 0f);
            vertices2[2].Position = new Vector3(-2f, -1f, 0f);
            vertices2[3].Position = new Vector3(0f, -1f, 0f);
            vertices2[4].Position = new Vector3(-2f, 1f, 0f);
            vertices2[5].Position = new Vector3(0f, 1f, 0f);

            vertices2[0].TextureCoordinate = new Vector2(0, 0);
            vertices2[1].TextureCoordinate = new Vector2(1, 1);
            vertices2[2].TextureCoordinate = new Vector2(0, 1);
            vertices2[3].TextureCoordinate = new Vector2(1, 1);
            vertices2[4].TextureCoordinate = new Vector2(0, 0);
            vertices2[5].TextureCoordinate = new Vector2(1, 0);
        }

        public void DrawGround()
        {
            CamAtVector = Vector3.Zero;
            CamUpVector = Vector3.UnitZ;
            
            basicEffect.TextureEnabled = true;
            basicEffect.Texture = floor;
            AspectRatio = Graphics.PreferredBackBufferWidth / (float)Graphics.PreferredBackBufferHeight;

            foreach (var pass in basicEffect.CurrentTechnique.Passes)
            {

                pass.Apply();
                Graphics.GraphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, floorParts, 0, 2);
            }
        }

        public void ScrollText()
        {
            if (ScrollTextLocation > -9999)
            {
                ScrollTextLocation-=10;
            }
        }

        public void Update(GameTime gameTime)
        {
            dayTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (attractMode)
            {
                slavFrame[1, slavAnim[0]] = 0;

                if (dayTimer > 3.0f)
                {
                    attractMode = false;
                    slavFrame[1, slavAnim[0]] = 1;
                }
            }
            else // Timers 
            {
                angle += 0.01f;
                if (angle > 9999)
                {
                    angle = 0;
                }

                for (int i = 0; i < 2; i++)
                {
                    timer[i] += timer[i + 2];
                }

                lerpingTime += 0.04f;

                if (dayTimer > 4.0f)
                {
                    DayTime = !DayTime;
                    dayTimer = 0;
                    lerpingTime = 0;
                }
            }

            for (int i = 0; i < 2; i++)
            {
                if (timer[i] > timerMax[i])
                {
                    if (slavAnim[i] < 7)
                    {
                        slavAnim[i]++;
                    }
                    else
                    {
                        slavAnim[i] = 0;
                    }
                    timer[i] = 0;
                }
            }

            if (DayTime) //day
            {
                if (dayTimer < 3.0f && sunMoonLocation < 450)
                {
                    sunMoonLocation += 10;
                }

                BGcolor = Color.Lerp(Color.MidnightBlue, Color.DeepSkyBlue, lerpingTime);
            }
            else //night
            {
                if (dayTimer < 3.0f && sunMoonLocation > 0)
                {
                    sunMoonLocation -= 10;
                }

                BGcolor = Color.Lerp(Color.DeepSkyBlue, Color.MidnightBlue, lerpingTime);
            }
        }


        public void DrawSprites (SpriteBatch SB)
        {
            //SB.Draw(moon, new Rectangle(200, sunMoonLocation, 150, 150), Color.White);
            SB.DrawString(font, ScrollingText, new Vector2(ScrollTextLocation, 700),Color.White);

            //for (int i = 0; i < 9; i++)
            //{
            //SB.Draw(tree, new Rectangle(150 * i, 100, 300, 360), Color.White);
            //}
        }

        public void Draw()
        {
            device.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, BGcolor, 1.0f, 0);
            DrawSprites(spriteBatch);

            basicEffect.Projection = projectionMatrix;
            basicEffect.View = viewMatrix;
            basicEffect.World = WorldMatrix;

            Matrix rotationMatrix = Matrix.CreateRotationZ(1 * angle);
            Vector3 transformedReference = Vector3.Transform(CamPosition, rotationMatrix);
            viewMatrix = Matrix.CreateLookAt(transformedReference, Camtarget, CamUpVector);
            Matrix rotationMatrix2 = Matrix.CreateRotationX(MathHelper.ToRadians(90f)) * rotationMatrix;

            DrawGround();

            effect.CurrentTechnique = effect.Techniques["Simplest"];
            effect.Parameters["xViewProjection"].SetValue(viewMatrix * projectionMatrix);
            effect.Parameters["xWorld"].SetValue(rotationMatrix2);
            effect.Parameters["xView"].SetValue(WorldMatrix);

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                effect.Parameters["xTexture"].SetValue(Slav1[slavFrame[0, slavAnim[0]]]);
                pass.Apply();
                device.DrawUserPrimitives(PrimitiveType.TriangleList, vertices, 0, 2, VertexPositionTexture.VertexDeclaration);

                effect.Parameters["xTexture"].SetValue(Slav2[slavFrame[1, slavAnim[1]]]);
                pass.Apply();
                device.DrawUserPrimitives(PrimitiveType.TriangleList, vertices2, 0, 2, VertexPositionTexture.VertexDeclaration); 
            }  
        }
    }
}
