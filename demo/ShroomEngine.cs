﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    class ShroomEngine
    {
        private Random random;
        public Vector2 EmitterLocation { get; set; }
        private List<ShroomParticle> particles;
        private List<Texture2D> textures;
        public bool angularSelect;

        public ShroomEngine(List<Texture2D> textures, Vector2 location, bool getAngle)
        {
            EmitterLocation = location;
            this.textures = textures;
            this.particles = new List<ShroomParticle>();
            this.angularSelect = getAngle;
            random = new Random();
        }

        private ShroomParticle GenerateNewParticle()
        {
            Texture2D texture = textures[random.Next(textures.Count)];
            Vector2 position = EmitterLocation;
            Vector2 velocity = new Vector2(
                    1f * (float)(random.NextDouble() * 2 - 1),
                    1f * (float)(random.NextDouble() * 2 - 1));
            float angle = 0;
            float angularVelocity = 0.1f * (float)(random.NextDouble() * 2 - 1);
            Color color = new Color(
                    (float)random.NextDouble(),
                    (float)random.NextDouble(),
                    (float)random.NextDouble());
            float size = 1;
            int ttl = 6;

            return new ShroomParticle(texture, position, velocity, angle, angularVelocity, color, size, ttl, angularSelect);
        }

        public void Update()
        {
            int total = 1;

            for (int i = 0; i < total; i++)
            {
                particles.Add(GenerateNewParticle());
            }

            for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                if (particles[particle].TTL <= 0)
                {
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int index = 0; index < particles.Count; index++)
            {
                particles[index].Draw(spriteBatch);
            }
        }
    }
}