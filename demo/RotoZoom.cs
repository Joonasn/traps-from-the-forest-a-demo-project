﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    public class ScrollCamera
    {
        public Vector2 Position = new Vector2(-100, 200), displacement;
        public float Zoom = 0.9f;
        public float Rotation, elapsed;
        public Vector2 Origin;
        public bool MaxZoom = true, RotationTurn = true;
        public int changeRotation, changeRotationMax = 2, turnsTaken;
        const float cameraTranslationSpeed = 1000f;
        public float cameraRotationSpeed = 4f;
        const float cameraZoomSpeed = 1f;
        public float MaxCameraZoom = 2f;
        public float minCameraZoom = 0.15f;

        public ScrollCamera(Viewport viewport)
        {
            Origin = new Vector2(viewport.Width / 2f, viewport.Height / 2f);
        }

        public Matrix GetScrollMatrix(Vector2 textureSize)
        {
            return Matrix.CreateTranslation(new Vector3(-Origin / textureSize, 0.0f)) *
                   Matrix.CreateScale(1f / Zoom) *
                   Matrix.CreateRotationZ(Rotation) *
                   Matrix.CreateTranslation(new Vector3(Origin / textureSize, 0.0f)) *
                   Matrix.CreateTranslation(new Vector3(Position / textureSize, 0.0f));
        }

        // Camera Translation
        // if (state.IsKeyDown(Keys.Left))
        // scrollCamera.Move(-Vector2.UnitX* elapsed * cameraTranslationSpeed, true);
        // if (state.IsKeyDown(Keys.Right))
        // scrollCamera.Move(Vector2.UnitX* elapsed * cameraTranslationSpeed, true);
        // if (state.IsKeyDown(Keys.Up))
        // scrollCamera.Move(-Vector2.UnitY* elapsed * cameraTranslationSpeed, true);
        // if (state.IsKeyDown(Keys.Down))
        // scrollCamera.Move(Vector2.UnitY* elapsed * cameraTranslationSpeed, true);

        public void Move(GameTime gametime)
        {
            elapsed = (float)gametime.ElapsedGameTime.TotalSeconds;

            //Movement
            displacement = Vector2.Transform(Vector2.UnitY * elapsed * cameraTranslationSpeed, Matrix.CreateRotationZ(Rotation));
            Position += displacement;

            if (RotationTurn) // Rotation
            {
                Rotation += cameraRotationSpeed * elapsed;
            }
            else
            {
                Rotation -= cameraRotationSpeed * elapsed;
            }

            if (changeRotation >= changeRotationMax)
            {
                RotationTurn = !RotationTurn;
                changeRotation = 0;

                if (changeRotationMax == 2)
                {
                    changeRotationMax = 3;
                }
                else
                {
                    changeRotationMax = 2;
                }
            }

            if (MaxZoom) // Zoom 
            {
                Zoom += cameraZoomSpeed * elapsed;
            }
            else
            {
                Zoom = MathHelper.Max(Zoom - cameraZoomSpeed * elapsed, minCameraZoom);
            }

            if (Zoom >= MaxCameraZoom || Zoom <= minCameraZoom)
            {
                MaxZoom = !MaxZoom;
                changeRotation++;
                turnsTaken++;
                elapsed = 0;
            }
        }

        public void Drop(GameTime gametime)
        {
            Zoom = MathHelper.Max(Zoom - cameraZoomSpeed * elapsed, minCameraZoom);
        }
    }
}