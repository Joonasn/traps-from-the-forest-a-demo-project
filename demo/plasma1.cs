﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    class plasma1
    {
        private int[][] palette = new int[256][];
        private int[][] plasma;
        private int paletteShift;
        private Texture2D texture;
        private double angle;
        Color[] colours;

        public plasma1(GraphicsDevice graphics, int number, int multiply, int endnumber, double chosenAngle)
        {
            texture = new Texture2D(graphics, 800, 500);
            colours = new Color[texture.Width * texture.Height];
            angle = chosenAngle;
            for (int x = 0; x < 256; x++)
            {
                palette[x] = new int[] {
                    (int)Math.Floor(128 +128 * Math.Sin(Math.PI * x / number)),
                    (int)Math.Floor(128 +128 * Math.Sin(Math.PI * x / (number*multiply))),
                    (int)Math.Floor(128 +128 * Math.Sin(Math.PI * x / endnumber))
                };
            }
            plasma = new int[texture.Width * texture.Height][];
            float zoom = .1f;
            for (int y = 0; y < texture.Height; y++)
            {
                int[] p = new int[texture.Width];
                for (int x = 0; x < texture.Width; x++)
                {
                    p[x] = (int)(128.0 + (128.0 * Math.Sin(Math.Sqrt((x-texture.Width/2.0) * (x-texture.Width/2.0)+ (y-texture.Height/2.0) * (y-texture.Height/2.0) / 8.0))));
                }

            plasma[y] = p;
            }
        }

        public void Update()
        {

            paletteShift += 6;
            for (int y = 0; y < texture.Height; y++)
            {
                for (int x = 0; x < texture.Width; x++)
                {
                    //if (x >= 150 && x <= texture.Width && y >= 155 && y <= 587)
                    //{
                    //continue;
                    //}
                    int[] c = palette[(plasma[y][x] + paletteShift) % 256];
                    colours[x + y * texture.Width] = new Color(c[0], c[1], c[2]);
                }
            }
            texture.SetData(colours);
        }

        public void Draw(SpriteBatch spriteBatch, int width, int height)
        {
            spriteBatch.Draw(texture, null, new Rectangle(0, 0, width, height));
        }
    }
}
