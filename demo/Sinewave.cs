﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    class Sinewave
    {
        float waveWait;
        bool showImage, showText = true;
        int sinScene, currentVoteImage, currentVoteMessage, textPos, ScreenWidth, ScreenHeight, logoY;
        Vector2 x_finder;
        List<SinWave> lines = new List<SinWave>();

        public Sinewave(Vector2 xFind, int screenWidth, int screenHeight)
        {
            ScreenHeight = screenHeight;
            ScreenWidth = screenWidth;
            x_finder = xFind;
            logoY = (ScreenHeight / 2) +600;

            for (int i = 0; i < screenHeight; i++)
            {
                lines.Add(new SinWave(new Rectangle(0, i, screenWidth, 1)));
                x_finder = (SinWave.RotateVector2(x_finder, 0.02f, Vector2.Zero));
                lines[i].my_destination = new Rectangle((int)x_finder.X, lines[i].my_destination.Y, lines[i].my_destination.Width, lines[i].my_destination.Height);
                lines[i].x_finder += x_finder;
                lines[i].my_from = (new Rectangle(0, i, screenWidth, 1));
            }
        }

        public void Update(GameTime gameTime)
        {
            if (sinScene < 4)
            {
                switch (sinScene)
                {
                    case 0: // Tuo kuva
                        textPos = 300;
                        showImage = true;
                        if (x_finder.X <= -40)
                        {
                            x_finder.X += 10;
                        }
                        if (x_finder.X >= -40)
                        {
                            waveWait += (float)gameTime.ElapsedGameTime.TotalSeconds;
                            if (waveWait >= 4.5f)
                            {
                                sinScene++;
                                waveWait = 0;
                            }
                        }
                        break;

                    case 1: // Häivytä kuva
                        waveWait += (float)gameTime.ElapsedGameTime.TotalSeconds;
                        x_finder.X -= 80;

                        if (waveWait > 1.0f)
                        {
                            waveWait = 0;
                            if (currentVoteImage < 1)
                            {
                                currentVoteImage++;
                                currentVoteMessage++;
                                sinScene = 0;
                            }
                            else
                            {
                                if (currentVoteMessage < 4)
                                {
                                    currentVoteImage = 2;
                                    currentVoteMessage++;
                                }
                                x_finder.X = -80;
                                sinScene++;
                            }
                        }
                        break;

                    case 2: //tekstit
                        textPos = 300;
                        showImage = true;
                        waveWait += (float)gameTime.ElapsedGameTime.TotalSeconds;

                        if (waveWait > 3.0f)
                        {
                            if (currentVoteMessage < 3)
                            {
                                currentVoteMessage++;
                            }
                            else
                            {
                                sinScene = 0;
                                showText = false;
                                currentVoteMessage = 4;
                            }
                            waveWait = 0;
                        }
                        break;
                }

                if (currentVoteMessage == 4 && logoY >650)
                {
                    logoY -= 5;
                }

                for (int i = 0; i < lines.Count; i++)
                {
                    x_finder = (SinWave.RotateVector2(x_finder, 0.02f, Vector2.Zero));
                    lines[i].my_destination = new Rectangle((int)x_finder.X, lines[i].my_destination.Y, lines[i].my_destination.Width, lines[i].my_destination.Height);
                    lines[i].x_finder = x_finder;
                    lines[i].my_from = (new Rectangle(0, i, ScreenWidth, 1));
                }

                foreach (SinWave line in lines)
                {
                    line.Update();
                }
            }
        }

        public void UpdateSimple (GameTime gametime)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                x_finder = (SinWave.RotateVector2(x_finder, 0.02f, Vector2.Zero));
                lines[i].my_destination = new Rectangle((int)x_finder.X, lines[i].my_destination.Y, lines[i].my_destination.Width, lines[i].my_destination.Height);
                lines[i].x_finder = x_finder;
                lines[i].my_from = (new Rectangle(0, i, ScreenWidth, 1));
            }

            foreach (SinWave line in lines)
            {
                line.Update();
            }
        }

        public void DrawSimple(SpriteBatch spriteBatch, Texture2D currentImage)
        {
            foreach (SinWave line in lines)
            {
                line.Draw(currentImage, spriteBatch);
            }
        }

        public void draw(SpriteBatch spriteBatch, Texture2D [] currentImage, Texture2D logo, string[] voteMessages, SpriteFont font2)
        {
            if (showImage)
            {
                foreach (SinWave line in lines)
                {
                    line.Draw(currentImage[currentVoteImage], spriteBatch);
                }
            }

            if (showText)
            {
                spriteBatch.DrawString(font2, "" + voteMessages[currentVoteMessage], new Vector2((ScreenWidth / 2) - (1500 / 2) + 9, ((ScreenHeight / 2) + textPos + 8)), Color.Black);
                spriteBatch.DrawString(font2, "" + voteMessages[currentVoteMessage], new Vector2((ScreenWidth / 2) - (1500 / 2), ((ScreenHeight / 2) + textPos)), Color.White);
            }

            if (currentVoteMessage == 4)
            {
                spriteBatch.Draw(logo, null,new Rectangle((ScreenWidth / 8)+100, logoY, 1362, 276));
            }
        }

        // Water Reflect

        public void UpdateVanilla(GameTime gameTime, int waterPos)
        {
            for (int i = 0; i < lines.Count; i++)
            {
                x_finder = (SinWave.RotateVector2(x_finder, 0.02f, Vector2.Zero));
                lines[i].my_destination = new Rectangle((int)x_finder.X, lines[i].my_destination.Y, lines[i].my_destination.Width, lines[i].my_destination.Height);
                lines[i].x_finder = x_finder;
                lines[i].my_from = (new Rectangle(0, i + (waterPos), ScreenWidth, 1));
            }

            foreach (SinWave line in lines)
            {
                line.Update();
            }
        }

        public void drawVanilla (SpriteBatch spriteBatch, Texture2D currentImg, int screenWidth, int screenHeight, Color vari)
        {
            foreach (SinWave line in lines)
            {
                line.DrawPos(currentImg, spriteBatch, screenWidth, screenHeight, vari);
            }
        }
    }
}