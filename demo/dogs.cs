﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    class dogs
    {
        SpriteFont font;
        SoundEffect walk;
        Texture2D dogi1, cop, sopulogo, shroomfield, park, headIMG;
        private int dogiFrame = 0, headFrame = 0, dogiAct = 0, currentDialog = 0, dogiXposition1 = 2000, flash, copFrame, currentLetter,scene, screenWidth, screenHeight;
        private float logoWait, headWait, guyWait, printTimer;
        private Vector2 dialogPos;
        private Rectangle dogiAnim, logoPosition, headAnim, copAnim;
        string printTxt;
        string[,] dialog = new string[2,6] { 
            { "Peaceful forest and poisonous shrooms... ", "The best combination for hallucinating! ","I'll show you why. Just watch!","","","" },
            { "Well, enough of that!", "Not the best hallucination i've had...  ", "But at least it ended well ", "...", "Voi vitt...", "Voi vitt..."}
        };

        public dogs (int Width, int Height, Texture2D dogimage, Texture2D copimage, Texture2D shrooms, Texture2D parkEnd,Texture2D sopu, Texture2D heads, Rectangle logoPos, SpriteFont fontti, SoundEffect sfx)
        {
            screenWidth = Width;
            screenHeight = Height;
            dogi1 = dogimage;
            cop = copimage;
            shroomfield = shrooms;
            park = parkEnd;
            sopulogo = sopu;
            headIMG = heads;
            logoPosition = logoPos;
            font = fontti;
            walk = sfx;
        }

        public void Update(GameTime gameTime)
        {
            scene = 0;
            dogiAnim = new Rectangle((306 * dogiFrame), 0, 306, 370);

            if (logoPosition.Y > 50) // LOGON LIIKUTTAMINEN
            {
                logoPosition.Y -= 5;
            }
            else
            {
                logoWait += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (logoPosition.Y > -850 && logoWait > 2.0f)
                {
                    logoPosition.Y -= 5;
                }
            }

            if (logoPosition.Y <= -850 && dogiAct == 0) // HAHMO SAAPUU
            {
                logoWait += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (logoWait > 0.5f)
                {
                    walk.Play();
                    if (dogiFrame < 1)
                    {
                        dogiFrame++;
                    }
                    else
                    {
                        dogiFrame = 0;
                    }
                    logoWait = 0;
                }

                if (dogiXposition1 > (shroomfield.Width -210))
                {
                    dogiXposition1 = dogiXposition1 - 10;
                }

                if (dogiXposition1 <= (shroomfield.Width - 210))
                {
                    if (dogiAct == 0)
                    {
                        logoWait = 0;
                        guyWait = 5.0f;
                    }
                    dialogPos.X = dogiXposition1 - 200;
                    dogiFrame = 2;
                    dogiAct = 1;
                }
            }

            if (dogiAct > 0) // DIALOGI STARTTAA
            {
                headAnim = new Rectangle((168 * headFrame), 0, 168, 113);
                logoWait += (float)gameTime.ElapsedGameTime.TotalSeconds;
                headWait += (float)gameTime.ElapsedGameTime.TotalSeconds;

                dialogPos.Y = logoPosition.Y + 1200;

                if (logoWait > guyWait)
                {
                    printTxt = "";
                    currentLetter = 0;
                    if (currentDialog < 4)
                    {
                        currentDialog++; 
                    }
                    logoWait = 0;
                }

                switch (currentDialog)
                {
                    case 0:
                    case 1:
                    case 2:
                        printDialog(logoWait);
                        guyWait = 4.0f;

                        if (headFrame > 4)
                        {
                            headFrame = 1;
                        }
                        break;

                    case 3:
                        if (logoWait > 1 && logoWait < 2)
                        {
                            dogiFrame = 3;
                        }
                        else if (logoWait > 2 && logoWait < 4)
                        {
                            dogiFrame = 4;
                        }
                        else if (logoWait >= guyWait)
                        {
                            guyWait = 4.0f;
                            headWait = 0;
                        }
                        break;

                    case 4:
                    default: // syönti
                        guyWait = 5.0f;
                        if (headWait > 0.3f)
                        {
                            dogiFrame++;
                            headWait = 0;
                        }
                        if (dogiFrame > 6)
                        {
                            dogiFrame = 5;
                        }
                        break;
                }
            }
        }

        public void draw (SpriteBatch spriteBatch, Color colors)
        {
            spriteBatch.Draw(sopulogo, logoPosition, colors);
            spriteBatch.Draw(shroomfield, new Vector2(logoPosition.X + 200, logoPosition.Y + 1000), Color.White);
            spriteBatch.Draw(dogi1, null, new Rectangle(dogiXposition1, logoPosition.Y + 1280, 306, 370), dogiAnim, null, 0.0f, null, null, SpriteEffects.None, 0.0f);

            if (dogiAct ==1) //DIALOG
            {
                spriteBatch.DrawString(font, "" + printTxt, dialogPos, Color.White);
                if (currentDialog < 2)
                {
                    spriteBatch.Draw(headIMG, null, new Rectangle(dogiXposition1 + 60, logoPosition.Y + 1290, 168, 113), headAnim, null, 0.0f, null, null, SpriteEffects.None, 0.0f);
                }
            }
        }

        public void printDialog(float timer)
        {
            printTimer += timer;
            if (printTimer >= 0.8f && currentLetter < dialog[scene, currentDialog].Length)
            {
                walk.Play();
                printTxt = dialog[scene, currentDialog].Substring(0,currentLetter);
                currentLetter++;
                headFrame++;
                printTimer = 0;
            }
        }

        public void reset()
        {
            logoWait = 0;
            guyWait = 0.5f;
            currentDialog = 0;
            dogiAct = 0;
            headFrame = 0;
            headWait = 0;
            dogiFrame = 3;     
        }

        // OUTRO 
        public void UpdateOutro(GameTime gameTime)
        {
            scene = 1;
            dogiAnim = new Rectangle((306 * dogiFrame), 0, 306, 370);
            headAnim = new Rectangle((168 * headFrame), 0, 168, 113);
            copAnim = new Rectangle((354 * copFrame), 0, 354, 405);

            logoWait += (float)gameTime.ElapsedGameTime.TotalSeconds;
            headWait += (float)gameTime.ElapsedGameTime.TotalSeconds;

            dialogPos = new Vector2 ((screenWidth / 2) - 250, (screenHeight / 2) - 90);
            
            if (logoWait > guyWait)
            {
                printTxt = "";
                currentLetter = 0;
                if (currentDialog < 6)
                {
                    currentDialog++;
                }
                logoWait = 0;
            }
            if (flash > 40)
            {
                flash = 0;
            }

            switch (currentDialog)
            {
                case 0: // alun dialogit 
                case 1:
                case 2:
                    guyWait = 4.0f;
                    printDialog(logoWait);
                    dogiFrame = 2;

                    if (headFrame > 4)
                    {
                        headFrame = 1;
                    }
                    
                    
                    break;

                case 3: // vilkkuminen
                    flash++;
                    if (flash > 20)
                    {
                        dogiFrame = 7;
                    }
                    else
                    {
                        dogiFrame = 2;
                    }
                    guyWait = 5.0f;
                    break;

                case 4: // katsominen ympärille
                    dogiFrame = 7;
                    if (logoWait < 1)
                    {
                        headFrame = 6;
                    }
                    else if (logoWait > 1 && logoWait < 3)
                    {
                        headFrame = 7;
                    }
                    else
                    {
                        headFrame = 8;
                    }
                    break;

                case 5: // Toteamus
                    guyWait = 0.7f;
                    printDialog(logoWait);

                    if (headFrame < 9 || headFrame > 10)
                    {
                        headFrame = 9;
                    }
                    break;
                case 6: // poliisi toimii
                    if (copFrame < 4)
                    {
                        copFrame++;
                        logoWait = 0;
                    }
                    if (copFrame==3)
                    {
                        dogiFrame = 8;
                    }
             
                    guyWait = 2.0f;
                    break;
                default:
                    break;
            }
        }

        public void drawOutro(SpriteBatch spriteBatch, Color colors)
        {
            if (currentDialog < 3)
            {
                spriteBatch.Draw(shroomfield, new Vector2((screenWidth / 2)-500, (screenHeight / 2)-350), Color.White);
            }
            else if (currentDialog==3)
            {
                if (flash > 20)
                {
                    spriteBatch.Draw(park, new Vector2((screenWidth / 2) - 500, (screenHeight / 2) - 350), Color.White);
                    spriteBatch.Draw(cop, null, new Rectangle((screenWidth / 2)-420, (screenHeight / 2)-80, 354, 405), copAnim, null, 0.0f, null, null, SpriteEffects.None, 0.0f);
                }
                else
                {
                    spriteBatch.Draw(shroomfield, new Vector2((screenWidth / 2) - 500, (screenHeight / 2) - 350), Color.White);
                }
            }
            else
            {
                spriteBatch.Draw(park, new Vector2((screenWidth / 2) - 500, (screenHeight / 2) - 350), Color.White);
                spriteBatch.Draw(cop, null, new Rectangle((screenWidth / 2) - 420, (screenHeight / 2) - 80, 354, 405), copAnim, null, 0.0f, null, null, SpriteEffects.None, 0.0f);
            }

            spriteBatch.Draw(dogi1, null, new Rectangle((screenWidth / 2) - 150, (screenHeight / 2) - 50, 306, 370), dogiAnim, null, 0.0f, null, null, SpriteEffects.None, 0.0f);

            if (currentDialog != 3 && currentDialog != 6)
            {
                spriteBatch.Draw(headIMG, null, new Rectangle((screenWidth / 2) - 85, (screenHeight / 2) - 40, 168, 113), headAnim, null, 0.0f, null, null, SpriteEffects.None, 0.0f);
            }
            spriteBatch.DrawString(font, "" + printTxt, dialogPos, Color.White);
        }


        public void UpdateHallu(GameTime gameTime, float movement)
        {
            headFrame = 11;
            dogiAnim = new Rectangle((306 * dogiFrame), 0, 306, 370);
            headAnim = new Rectangle((168 * headFrame), 0, 168, 113);
            
            if (movement > 2 && dogiXposition1 > -270) // HAHMO POISTUU RUUDULTA
            {
                logoWait += (float)gameTime.ElapsedGameTime.TotalSeconds;
                dogiXposition1 = dogiXposition1 - 10;

                if (logoWait > 0.5f)
                {
                    walk.Play();
                    if (dogiFrame < 1)
                    {
                        dogiFrame++;
                    }
                    else
                    {
                        dogiFrame = 0;
                    }
                    logoWait = 0;
                }
            }
            else
            {
                dogiFrame = 2;
            }
        }

        public void drawHallu(SpriteBatch spriteBatch, Color colors)
        {
            spriteBatch.Draw(dogi1, null, new Rectangle(dogiXposition1, logoPosition.Y + 1280, 306, 370), dogiAnim, null, 0.0f, null, null, SpriteEffects.None, 0.0f);
            spriteBatch.Draw(headIMG, null, new Rectangle(dogiXposition1 + 60, logoPosition.Y + 1290, 168, 113), headAnim, null, 0.0f, null, null, SpriteEffects.None, 0.0f);
        }
    }
}