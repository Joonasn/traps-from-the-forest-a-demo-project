float4x4 xViewProjection;
float4x4 xView;
float4x4 xWorld;

Texture xTexture;
float3 xCamPos;


sampler TextureSampler = sampler_state { texture = <xTexture> ; magfilter = LINEAR; minfilter = LINEAR; mipfilter=LINEAR; AddressU = mirror; AddressV = mirror;};
struct VertexToPixel
{
    float4 Position     : POSITION;  
    float2 TexCoords    : TEXCOORD0;
};

struct PixelToFrame
{
    float4 Color        : COLOR0;
};

 VertexToPixel SimplestVertexShader( float4 inPos : POSITION, float2 inTexCoords : TEXCOORD0)
 {
     VertexToPixel Output = (VertexToPixel)0;

	 float4x4 preViewProjection = mul(xView, xViewProjection);
	 float4x4 preWorldViewProjection = mul(xWorld, preViewProjection);
     
     Output.Position =mul(inPos, preWorldViewProjection);
     Output.TexCoords = inTexCoords;

     return Output;
 }
 
 PixelToFrame OurFirstPixelShader(VertexToPixel PSIn)
 {
    PixelToFrame Output = (PixelToFrame)0;    
 
    Output.Color = tex2D(TextureSampler, PSIn.TexCoords);

	clip(Output.Color < float4 (0.0f, 0.0f, 0.0f, 1.0f) ? -1 : 1);

    return Output;
}

technique Simplest
{
    pass Pass0
    {	
        VertexShader = compile vs_4_0_level_9_1 SimplestVertexShader();
        PixelShader = compile ps_4_0_level_9_1  OurFirstPixelShader();
    }
}