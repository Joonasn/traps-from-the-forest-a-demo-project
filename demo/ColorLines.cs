﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    class ColorLines
    {
        Texture2D lineTexture;
        Random lineRand = new Random();
        int movementWay, linesize=50, LineMax;
        bool lineWay=true;
        Color[] startColors;
        int[,] directionSpeed = new int[,] { { 20, 15, 30, 60 }, { -10, -15, -30, -60 }, { 20, 15, 30, 60 } };
        int[,] limits = new int[,] { { 200,-100}, { 50, -1100 }, { 100, -200 } };                              
                                        // Pysty                Oikea               vasen                   Vaaka
        Vector2[] start = new Vector2[]{new Vector2(0, -300), new Vector2(0, -300), new Vector2(0, -300), new Vector2(-2000, 0)};
        Vector2[] end = new Vector2[]{new Vector2(0, 1700), new Vector2(1000, 1700), new Vector2(-1000, 1700), new Vector2(2000,0)};
        Vector2[] movement = new Vector2[] { new Vector2(10, 0), new Vector2(50, 0), new Vector2(30, 0), new Vector2(100, 0)};

        public ColorLines (GraphicsDevice graphics)
        {
            lineTexture = new Texture2D(graphics, 500, 10);
            Color[] data = new Color[500 * 10];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = Color.White;
                lineTexture.SetData(data);
            }
        }

        public void Update(Color blend1, Color blend2, Color blend3)
        {
            startColors = new Color[] { blend1, blend2, blend3 };
            if (lineWay)
            {
                linesize++;
            }
            else
            {
                linesize--;
            }

            if (linesize < 20 || linesize > LineMax)
            {
                LineMax = lineRand.Next(200);
                lineWay = !lineWay;
            }
            
            movementWay++;
            for (int i = 0; i < 3; i++) // Piirretään jokainen viiva
            {
                movement[i].X += directionSpeed[0,i];

                if (movement[i].X > limits[i, 0])
                {
                    if (directionSpeed[0, i] > directionSpeed[1, i])
                    {
                        directionSpeed[0, i]--;
                    }
                }
                if (movement[i].X < limits[i, 1])
                {
                    if (directionSpeed[0, i] < directionSpeed[2, i])
                    {
                        directionSpeed[0, i]++;
                    }
                }
                if (movementWay > 2000)
                {
                    movementWay = 0;
                }
                movement[i].Y += Convert.ToInt32(5 * Math.Sin(movementWay * 0.5 * 2 / 20));
            }
        }

        public void Draw(SpriteBatch SB)
        {
            for (int i = 0; i < 3; i++) // viivaryhmät
            {
                for (int j = 0; j < 20; j++) // Piirretään jokainen viiva
                {
                    Vector2 edge = end[i] - start[i];
                    // calculate angle to rotate line
                    float angle = (float)Math.Atan2(edge.Y, edge.X);

                    SB.Draw(lineTexture,
                        new Rectangle(// rectangle defines shape of line and position of start of line
                            (int)(start[i].X + 200 * j) + (int)movement[i].X,
                            (int)start[i].Y + (int)movement[i].Y,
                            (int)edge.Length(), //sb will strech the texture to fill this rectangle
                            linesize), //width of line, change this to make thicker line
                        null,
                        startColors[i], //colour of line
                        angle,     //angle of line (calulated above)
                        new Vector2(0, 0), // point in line about which to rotate
                        SpriteEffects.None,
                        0);
                }
            }
        }
    }
}