﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    public class SinWave
    {
        public Rectangle my_from;
        public Rectangle my_destination;
        public Vector2 x_finder;

        public SinWave(Rectangle start)
        {
            my_destination = start;
        }

        public static Vector2 RotateVector2(Vector2 point, float radians, Vector2 pivot)
        {
            float cosRadians = (float)Math.Cos(radians);
            float sinRadians = (float)Math.Sin(radians);

            Vector2 translatedPoint = new Vector2();
            translatedPoint.X = point.X - pivot.X;
            translatedPoint.Y = point.Y - pivot.Y;

            Vector2 rotatedPoint = new Vector2();
            rotatedPoint.X = translatedPoint.X * cosRadians - translatedPoint.Y * sinRadians + pivot.X;
            rotatedPoint.Y = translatedPoint.X * sinRadians + translatedPoint.Y * cosRadians + pivot.Y;

            return rotatedPoint;
        }

        public void Update()
        {
            x_finder = RotateVector2(x_finder, 0.04f, Vector2.Zero);
            my_destination.X = ((int)x_finder.X)/2;
        }

        public void Draw(Texture2D image,SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(image, new Rectangle (my_destination.X + 450, (my_destination.Y/2)+170, 2000,10 ), my_from, new Color(my_destination.X*20,100,my_destination.Y/4), 0, Vector2.Zero, SpriteEffects.None, 0);
        }

        public void DrawPos(Texture2D image, SpriteBatch spriteBatch, int screenWidth, int screenHeight, Color vari)
        {
            spriteBatch.Draw(image, new Rectangle(my_destination.X -100, (my_destination.Y / 2) + 705, screenWidth+200, screenHeight), my_from, vari,0,Vector2.Zero, SpriteEffects.None, 0);
        }
    }
}