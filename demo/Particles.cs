﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    class Particles
    {
        Texture2D texture;
        float spawnWidth, density;
        List<Fireworks> fireworks = new List<Fireworks>();
        Random random1 = new Random();

        public Particles(GraphicsDevice graphics, float newspawn, float newdest)
        {
            texture = new Texture2D(graphics, 50, 50);
            Color[] data = new Color[50 * 50];
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = Color.Chocolate;
                texture.SetData(data);
            }
            spawnWidth = newspawn;
            density = newdest;
        }
        public void createParticle()
        {
            fireworks.Add(new Fireworks(texture, new Vector2(100,100), new Vector2 (random1.Next(-10, 30), random1.Next(-10,30))));
        }

        public void Update (GraphicsDevice graphics)
        {

            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                for (int i = 0; i < 50; i++)
                {
                    createParticle();
                }
            }

            for(int i=0; i <fireworks.Count; i++)
            {
                fireworks[i].Update();

                if (fireworks[i].FirePosition.Y > graphics.Viewport.Height)
                {
                    fireworks.RemoveAt(i);
                    i--;
                }
            }
        }

        public void Draw (SpriteBatch spriteBatch, Color current)
        {
            foreach (Fireworks firework in fireworks)
            {
                firework.Draw(spriteBatch, current);
            }
        }
    }

    class Fireworks
    {
        Texture2D fireTexture;
        Vector2 firePosition, velocity;

        public Vector2 FirePosition
        {
            get { return firePosition; }
        }

        public Fireworks(Texture2D newTexture, Vector2 newPosition, Vector2 newVelocity)
        {
            fireTexture = newTexture;
            firePosition = newPosition;
            velocity = newVelocity;
        }

        public void Update()
        {
            firePosition += velocity;
        }

        public void Draw(SpriteBatch spritebatch, Color current)
        {
            spritebatch.Draw(fireTexture, firePosition, current);
        }
    }
}