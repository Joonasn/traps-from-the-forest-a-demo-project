﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace demo
{

    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch, spriteBatchZoom, spriteBatchBlend, MirrorEffect, MirrorEffect2, MirrorEffect3, spriteBatchBehind;
        SpriteFont font, font2;
        Colors colors, blend1,blend2,blend3;
        dogs introDogs;
        ScrollCamera scrollCamera;
        Effect rotozooming;
        Particles firework;
        Sinewave sinewave, waterWave, Sopuwants;
        plasma3 plasma,plasma2, introplasma1, introplasma2, introplasma3;
        _3Dpaska kolmeDeesonta; 
        ColorLines ClashLines;
        RenderTarget2D WaterEffect, TextEffect;
        List<SoundEffect> SFX = new List<SoundEffect>();
        List<Texture2D> Shrooms= new List<Texture2D>();
        ShroomEngine Shroomparticles;
        Random randColor = new Random();
        BasicEffect basicEffect;

        Effect effect;
        Texture2D floor, moon, tree;
        Texture2D[] Slav1 = new Texture2D[3];
        Texture2D[] Slav2 = new Texture2D[6];
        Texture2D sopulogo, dogi1, shroomfield, parkEnd, bootlegFerris, trapsFromTheForest, poliisi, head, night, highFall;
        Texture2D[] vote = new Texture2D[3];
        Rectangle logoPosition, textureRec;
        Vector2 textureORG;
        Vector2[,] CreditsYX;
        Song Demosong;

        bool musicSETUP=false;
        int screenWidth = 1920, screenHeight = 950, demoScene=0, waterwaving, waterPos, moveAway, minimize=9000, lowerfallspeed;
        float waveWait, changeScene, changeSceneMax = 10, TextureRotation;
        float[] TitlecardColors = new float[3];
        string[] voteMessages = new string[] { "WINNERS don't do drugs...", "WINNERS use shrooms!", "BUT MOST IMPORTANTLY", "WINNERS VOTE FOR..."," "};

        string[] credits = new string[] { "Traps From The Forest", "by", "SopuisaSopuli"," ", "Presented At", "Instanssi 2019"," ", "***",
            "Greetings to:"," ", "Mikko K from Joensuu", "You know why..."," "," ","Developers of Monogame"," "," ","The guy who handed Instanssi ad label", "to my friend at Tracon '18"," "," ",
            "And last, but not least...", "Everyone at Instanssi '19"," "," ", "Thank you for watching!"," ","And remember..."};
       
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;
            graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            //Demo setup 
            CreditsYX = new Vector2[credits.Length, credits.Length];
            colors = new Colors(0.18f, true, Color.Red, Color.Orange, Color.Yellow, Color.Green, Color.Blue, Color.Indigo, Color.Violet);
            blend1 = new Colors(0.18f, true, Color.Green, Color.Red, Color.Purple);
            blend2 = new Colors(0.18f, true, Color.DarkSlateBlue, Color.DarkSeaGreen);
            blend3 = new Colors(0.18f, true, Color.DarkTurquoise, Color.DarkSlateGray);

            sinewave = new Sinewave(new Vector2(-700, 0), screenWidth, screenHeight);
            waterWave = new Sinewave(new Vector2(25, 0), screenWidth, screenHeight);
            Sopuwants = new Sinewave(new Vector2(100, 0), screenWidth, screenHeight);
            introDogs = new dogs(screenWidth, screenHeight,dogi1, poliisi, shroomfield, parkEnd, sopulogo, head, logoPosition, font, SFX[0]);
            Shroomparticles = new ShroomEngine(Shrooms, new Vector2(200, 240), false);

            basicEffect = new BasicEffect(GraphicsDevice);
            kolmeDeesonta = new _3D(moon, floor, tree, font2, effect, Slav1, Slav2, graphics, spriteBatch);
            introplasma1 = new plasma3(graphics.GraphicsDevice, 320, 4, 1, 100.0,0);
            introplasma2 = new plasma3(graphics.GraphicsDevice, 320, 8, 2, 100.0,0);
            introplasma3 = new plasma3(graphics.GraphicsDevice, 320, 32, 4, 100.0,0);
            plasma = new plasma3(graphics.GraphicsDevice, 32,32,1, 100.0,1);
            plasma2 = new plasma3(graphics.GraphicsDevice, 320,3,60, 2.5,1);

            firework = new Particles(graphics.GraphicsDevice, graphics.GraphicsDevice.Viewport.Width, 100);
            ClashLines = new ColorLines(graphics.GraphicsDevice);
            WaterEffect = new RenderTarget2D(GraphicsDevice,screenWidth,screenHeight,false,GraphicsDevice.PresentationParameters.BackBufferFormat,DepthFormat.Depth24);
            TextEffect = new RenderTarget2D(GraphicsDevice, screenWidth, screenHeight, false, GraphicsDevice.PresentationParameters.BackBufferFormat, DepthFormat.Depth24);
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteBatchZoom = new SpriteBatch(GraphicsDevice);
            spriteBatchBlend = new SpriteBatch(GraphicsDevice);
            MirrorEffect = new SpriteBatch(GraphicsDevice);
            MirrorEffect2 = new SpriteBatch(GraphicsDevice);
            MirrorEffect3 = new SpriteBatch(GraphicsDevice);
            spriteBatchBehind = new SpriteBatch(GraphicsDevice);
            font = this.Content.Load<SpriteFont>("font");
            font2 = this.Content.Load<SpriteFont>("font2");

            Demosong = this.Content.Load<Song>("test");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Volume = 0.7f;

            sopulogo = this.Content.Load<Texture2D>("logo");
            dogi1 = this.Content.Load<Texture2D>("opettaja");
            poliisi = this.Content.Load<Texture2D>("poliisi");
            head = this.Content.Load<Texture2D>("heads");
            shroomfield = this.Content.Load<Texture2D>("sienipelto");
            parkEnd = this.Content.Load<Texture2D>("puisto");
            logoPosition = new Rectangle((screenWidth / 2) - (1362 / 2), (screenHeight / 2) + 500, 1362, 207);
            
            bootlegFerris = this.Content.Load<Texture2D>("ferris2");
            trapsFromTheForest = this.Content.Load<Texture2D>("sienet");
            night = this.Content.Load<Texture2D>("credits");
            highFall = this.Content.Load<Texture2D>("highonshrooms");

            floor = this.Content.Load<Texture2D>("floor");
            moon = this.Content.Load<Texture2D>("moon");

            for (int i = 0; i < 3; i++)
            {
                Slav1[i] = Content.Load<Texture2D>("slav" + (i + 1));
            }

            for (int i = 0; i < 6; i++)
            {
                Slav2[i] = Content.Load<Texture2D>("slav2" + (i + 1));
            }

            tree = Content.Load<Texture2D>("puu");
            effect = Content.Load<Effect>("texture");

            for (int i = 0; i < 3; i++)
            {
                vote[i] = this.Content.Load<Texture2D>("viineri" + (i + 1));
            }

            for (int i=0;i<1; i++)
            {
                Shrooms.Add(vote[1]);
            }

            SFX.Add(Content.Load<SoundEffect>("walkingsound"));
    
            scrollCamera = new ScrollCamera(GraphicsDevice.Viewport);
            rotozooming = Content.Load<Effect>("infinite");
            rotozooming.Parameters["ViewportSize"].SetValue(new Vector2(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height));
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }
            
            firework.Update(graphics.GraphicsDevice);
            changeScene += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (changeScene >= changeSceneMax)
            {
                demoScene++;
                changeScene = 0;
            }
            blend1.Update(gameTime); ////viivat
            blend2.Update(gameTime);
            blend3.Update(gameTime);

            if (musicSETUP)
            {
                MediaPlayer.Play(Demosong);
            }

            switch (demoScene)
            {
                case 0: // Intro
                    
                    changeSceneMax = 25;
                    colors.Update(gameTime);
                    introDogs.Update(gameTime);
                    
                    if (changeScene > 19 && changeScene < 20)
                    {
                        introplasma1.Update();
                    }
                    else if (changeScene > 20 && changeScene < 22)
                    {
                        introplasma2.Update();
                    }
                    else if (changeScene > 22)
                    {
                        introplasma3.Update();

                        if (changeScene < 24)
                        {
                            musicSETUP = true;
                        }
                        else
                        {
                            musicSETUP = false;
                        }  
                    }
                    break;

                case 1:  // "SopuisaSopuli presents"
                    introDogs.UpdateHallu(gameTime, changeScene);
                    changeSceneMax = 12;
                    minimize = 9000;
                    plasma.Update(); // Efekti (plasma, patterni vaihtelee)

                    //Shroomparticles.EmitterLocation = new Vector2(3, 3);
                    //Shroomparticles.Update();

                    //for (int i=0; i < TitlecardColors.Length; i++)
                    //{
                        //TitlecardColors[i] = (float)randColor.NextDouble();
                    //}
                    break;

                case 2: // Rotozoomer [TEHTY]
                    introDogs.reset();
                    changeSceneMax = 15;
                    TextureRotation += 0.2f;

                    textureRec = new Rectangle((screenWidth / 2), screenHeight / 2, minimize, minimize);
                    textureORG = new Vector2(textureRec.X/2, textureRec.Y/2);

                    if (changeScene > 3)
                    {
                        scrollCamera.Move(gameTime);
                    }

                    if (changeScene > 8)
                    {
                        if (lowerfallspeed < 30)
                        {
                            lowerfallspeed++;
                        }
                        if (minimize > 0)
                        {
                            minimize -= (100 - lowerfallspeed);
                        }
                    }
                    break;

                case 3: // Sine
                    plasma2.Update();
                    changeSceneMax = 15;
                    sinewave.Update(gameTime);
                    break;

                case 4: //3D
                    changeSceneMax = 18;
                    kolmeDeesonta.Update(gameTime);

                    if (changeScene > 1 && changeScene < 5)
                    {
                        kolmeDeesonta.UpdateCamera();
                    }

                    if (changeScene > 5)
                    {
                        kolmeDeesonta.ScrollText();
                    }

                    break;

                case 5: //credits
                    changeSceneMax = 40;
                    introDogs.reset();
                    waterWave.UpdateVanilla(gameTime, waterPos);

                    waterwaving++;
                    if (waterwaving >2000)
                    {
                        waterwaving = 0;
                    }
                    waterPos += Convert.ToInt32(1 * Math.Sin(waterwaving * 1.5 * 2 / 20));

                    if (changeScene > 35)
                    {
                        Sopuwants.UpdateVanilla(gameTime, 0);
                        ClashLines.Update(blend1.CurrentColor, blend2.CurrentColor, blend3.CurrentColor);
                    }
                    break;

                case 6: //Outro
                    MediaPlayer.Stop();
                    musicSETUP = false;
                    changeSceneMax = 25;
                    introDogs.UpdateOutro(gameTime);
                    break;
                case 7:
                    Exit();
                    break;
                default:
                    break;
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            rotozooming.Parameters["ScrollMatrix"].SetValue(scrollCamera.GetScrollMatrix(new Vector2(bootlegFerris.Width, bootlegFerris.Height)));
            Vector2 Middle = new Vector2((screenWidth / 2) - 50, screenHeight / 2);

            spriteBatch.Begin();
            switch (demoScene)
            {
                case 0: // intro

                    if (changeScene > 19 && changeScene < 20)
                    {
                        introplasma1.Draw(spriteBatch, screenWidth, screenHeight);
                    }
                    else if (changeScene > 20 && changeScene < 22)
                    {
                        introplasma2.Draw(spriteBatch, screenWidth, screenHeight);
                    }
                    else if (changeScene > 22)
                    {
                        introplasma3.Draw(spriteBatch, screenWidth, screenHeight);
                    }
                    introDogs.draw(spriteBatch, colors.CurrentColor);
                    break;

                case 1: // "SopuisaSopuli presents" 
                    plasma.Draw(spriteBatch, screenWidth, screenHeight);
                    introDogs.drawHallu(spriteBatch, Color.White);
                    Shroomparticles.Draw(spriteBatch);

                    if (changeScene > 6)
                    {
                        spriteBatch.Draw(trapsFromTheForest, null, new Rectangle(0, 0, screenWidth, screenHeight));
                    }
                    break;

                case 2: // Rotozoomer 
                    spriteBatchZoom.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null, rotozooming);
                        spriteBatchZoom.Draw(bootlegFerris, GraphicsDevice.Viewport.Bounds, GraphicsDevice.Viewport.Bounds, Color.White);
                    spriteBatchZoom.End();

                    if (changeScene < 3)
                    {
                        if (moveAway > -screenHeight)
                        {
                            moveAway -= 10;
                        }
                        spriteBatch.Draw(trapsFromTheForest, null, new Rectangle(0, 0 + moveAway, screenWidth, screenHeight));
                    }
                    
                    if (minimize > 0 && changeScene > 15)
                    { 
                        spriteBatch.Draw(highFall, null, textureRec, null, textureORG, TextureRotation, null, Color.White, SpriteEffects.None, 0.0f);
                    }
                    break;

                case 3: // Sinewave
                    plasma2.Draw(spriteBatch, screenWidth, screenHeight);
                    sinewave.draw(spriteBatch, vote, sopulogo, voteMessages, font2);
                    break;

                case 4:  // 3D-Effu

                    for (int i = 0; i < credits.Length; i++)
                    {
                        CreditsYX[1, i] = font.MeasureString(credits[i]);
                        CreditsYX[0, i] = new Vector2(CreditsYX[1, i].X / 2, (CreditsYX[1, i].Y / 2) - 50 * i);
                    }
                    kolmeDeesonta.Draw(); //Joo'o...
                    kolmeDeesonta.Draw();
                    break;

                case 5:  // Credits
                    GraphicsDevice.SetRenderTarget(TextEffect);
                    MirrorEffect3.Begin();
                    for (int i = 0; i < credits.Length; i++)
                    {
                        CreditsYX[0, i].Y++;
                      
                        if (CreditsYX[0,i].Y < 800) 
                        {
                            MirrorEffect3.DrawString(font, "" + credits[i], new Vector2((int)Middle.X - CreditsYX[0, i].X, (int)Middle.Y - CreditsYX[0,i].Y + 200), Color.White);
                        }
                    }
                    MirrorEffect3.End();

                    if (changeScene > 35)
                    {
                        spriteBatchBlend.Begin(SpriteSortMode.Immediate, BlendState.Additive);
                            ClashLines.Draw(spriteBatchBlend);
                        spriteBatchBlend.End();

                        spriteBatchBehind.Begin();
                            Sopuwants.DrawSimple(spriteBatchBehind, vote[2]);
                        spriteBatchBehind.End();

                        spriteBatch.DrawString(font2, " VOTE SOPUISASOPULI!!!", new Vector2((int)Middle.X - 795, (int)Middle.Y + 295), Color.Black);
                        spriteBatch.DrawString(font2, " VOTE SOPUISASOPULI!!!", new Vector2((int)Middle.X - 800, (int)Middle.Y + 300), Color.White);
                    }
                    GraphicsDevice.SetRenderTarget(null);
                    GraphicsDevice.SetRenderTarget(WaterEffect);

                    MirrorEffect.Begin();
                        MirrorEffect.Draw(TextEffect, new Vector2(0, -245), null, null, null, 0.0f, null, null, SpriteEffects.FlipVertically, 0.0f);
                        MirrorEffect.Draw(night, null, new Rectangle(0, 0, screenWidth, screenHeight), null, null, 0.0f, null, null, SpriteEffects.FlipVertically, 0.0f);
                    MirrorEffect.End();
                    GraphicsDevice.SetRenderTarget(null);

                    MirrorEffect2.Begin();
                        //MirrorEffect2.Draw(WaterEffect, null, new Rectangle(0,700, screenWidth, screenHeight+ waterPos), null, null, 0.0f, null, Color.Blue, SpriteEffects.FlipVertically,0.0f);
                        waterWave.drawVanilla(MirrorEffect2, WaterEffect, screenWidth, screenHeight, Color.Blue);
                        MirrorEffect2.Draw(WaterEffect, new Vector2(0, -245), null, null, null, 0.0f, null, null, SpriteEffects.FlipVertically, 0.0f);  
                    MirrorEffect2.End();
                    break;

                case 6: //Outro
                    introDogs.drawOutro(spriteBatch, colors.CurrentColor);
                    break;
                default:
                    break;
            }
            // firework.Draw(spriteBatch, colors.CurrentColor);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}