﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    class ShroomParticle
    {
        public Texture2D Texture;
        public Vector2 Position;
        public Vector2 Velocity;
        public float Angle;
        public float AngularVelocity;
        public Color Color;
        public float Size, perse, perse2;
        public int TTL;
        public bool isAngleOn;    
        
        public ShroomParticle(Texture2D texture, Vector2 position, Vector2 velocity,float angle, float angularVelocity, Color color, float size, int ttl, bool angularSelect)
        {
            Texture = texture;
            Position = position;
            Velocity = velocity;
            Angle = angle;
            AngularVelocity = angularVelocity;
            Color = color;
            Size = size;
            TTL = ttl;
            isAngleOn = angularSelect;
        }        
        public void Update()
        {
            TTL--;
            Position += Velocity;



            if (isAngleOn)
            {
                Angle += AngularVelocity;
            }  
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            Rectangle sourceRectangle = new Rectangle(0, 0, Texture.Width, Texture.Height);
            Vector2 origin = new Vector2(Texture.Width, Texture.Height);

            perse = Position.X;
            perse2 = Position.Y;
            spriteBatch.Draw(Texture, new Rectangle((int)perse, (int)perse2, 150, 150), Color);
            //spriteBatch.Draw(Texture, Position, sourceRectangle, Color,Angle, origin, Size, SpriteEffects.None, 0f);
        }
    }
}